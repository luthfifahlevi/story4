from django.shortcuts import render

# Create your views here.
def beranda(request):
    return render(request, 'beranda.html')

def profil(request):
    return render(request, 'profil.html')

def tentangSaya(request):
    return render(request, 'tentangSaya.html')
