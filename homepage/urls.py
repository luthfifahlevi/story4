from django.urls import path
from . import views

app_name = 'homepage'

urlpatterns = [
    path('', views.beranda, name='beranda'),
    path('profil', views.profil, name='profil'),
    path('tentang_saya',views.tentangSaya, name='tentangSaya')
]