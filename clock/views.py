from django.shortcuts import render
import datetime

# Create your views here.
def clock(request):
    jam = datetime.datetime.now()
    return render(request, 'clock.html', {"jam":jam})