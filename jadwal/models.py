from django.db import models
from django.contrib.auth.models import User

class Schedule(models.Model):
    matkul = models.CharField(max_length = 100)
    dosen = models.CharField(max_length = 100)
    jumlah_sks = models.PositiveIntegerField(default=1)
    deskripsi_matkul = models.TextField()

    LIST_TAHUN_SEMESTER = {
        ('2020/2021', '2020/2021'),
        ('2021/2022', '2021/2022'),
        ('2022/2023', '2022/2023'),
        ('2023/2024', '2023/2024'),
    }
    tahun_semester = models.CharField(
        max_length=100,
        choices = LIST_TAHUN_SEMESTER,
        default = '2020/2021',
        )
    ruang_kelas = models.CharField(max_length = 100)

class Tugas(models.Model):
    nama_tugas = models.CharField(max_length=100)
    deadline = models.DateField()
    jadwal = models.ForeignKey(Schedule, on_delete=models.CASCADE)