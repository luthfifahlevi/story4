# Generated by Django 3.0.3 on 2020-02-28 12:02

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('jadwal', '0008_auto_20200228_1841'),
    ]

    operations = [
        migrations.AlterField(
            model_name='schedule',
            name='tahun_semester',
            field=models.CharField(choices=[('2023/2024', '2023/2024'), ('2022/2023', '2022/2023'), ('2020/2021', '2020/2021'), ('2021/2022', '2021/2022')], default='2020/2021', max_length=100),
        ),
    ]
