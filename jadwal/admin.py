from django.contrib import admin

# Register your models here.
from .models import Schedule, Tugas

admin.site.register(Schedule)
admin.site.register(Tugas)