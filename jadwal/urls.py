from django.urls import path
from . import views
app_name = 'jadwal'

urlpatterns = [
    path('', views.tampilan, name='tampilan'),
    path('atur/', views.jadwal, name='jadwal'),
    path('delete/<int:delete_id>', views.delete, name='delete'),
    path('detail/<int:detail_id>', views.detail, name='detail'),
]