from django.shortcuts import render, redirect
from .forms import Schedule_Form
from .models import Schedule, Tugas

def tampilan(request):
    schedule = Schedule.objects.all()

    context = {
        'semua_schedule':schedule
    }
    return render(request, 'tampilan.html', context)

def jadwal(request):
    schedule_form = Schedule_Form(request.POST or None)
    if request.method == "POST":
        if schedule_form.is_valid():
            schedule_form.save()

        return redirect('jadwal:tampilan')
    
    context = {
        'schedule_form' : schedule_form
    }
    return render(request, 'jadwal.html', context)

def delete(request, delete_id):
    Schedule.objects.filter(id = delete_id).delete()
    return redirect('jadwal:tampilan')

def detail(request, detail_id):
    jenis = Schedule.objects.get(pk = detail_id)
    semua_tugas = Tugas.objects.filter(jadwal = jenis)
    return render(request, 'detail.html', {'jenis': jenis, 'semua_tugas':semua_tugas})