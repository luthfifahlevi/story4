from django import forms
from .models import Schedule
# Create your views here.

class Schedule_Form(forms.ModelForm):
    class Meta:
        model = Schedule
        fields = "__all__"
        labels = {
            'matkul' : 'Mata Kuliah',
            'dosen' : 'Dosen Pengajar',
            'jumlah_sks' : 'Jumlah SKS',
            'deskripsi_matkul' : 'Deskripsi Mata Kuliah',
            'tahun_semester': 'Tahun Semester',
            'ruang_kelas' : 'Ruang Kelas',
        }
    
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({
                'class': 'form-control'
        }
        )