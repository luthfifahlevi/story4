from django.test import TestCase, Client
from django.urls import resolve
from .views import tampilan1, kegiatan, peserta, detail1, delete1, delete2
from .models import Kegiatan, Peserta
from .forms import Activities_Form, Peserta_Form
from .apps import KegiatanConfig

# Create your tests here.
class TestRouting(TestCase):
    def test_event_url_is_exist(self):
        response = Client().get('/kegiatan/')
        self.assertEqual(response.status_code, 200)
    def test_event2_url_is_exist(self):
        response = Client().get('/kegiatan/atur_kegiatan/')
        self.assertEqual(response.status_code, 200)
    def test_event3_url_is_exist(self):
        kegiatannya = Kegiatan.objects.create(nama_kegiatan="Lari", deskripsi_kegiatan="Lari kuy",tanggal_kegiatan="2020-03-10")
        idnya = kegiatannya.id
        response = Client().get('/kegiatan/detail/' + str(idnya))
        self.assertEqual(response.status_code, 200)
    def test_event4_url_is_exist(self):
        kegiatannya = Kegiatan.objects.create(nama_kegiatan="Lari", deskripsi_kegiatan="Lari kuy",tanggal_kegiatan="2020-03-10")
        idnya = kegiatannya.id
        response = Client().get('/kegiatan/atur_peserta/' + str(idnya) + "/")
        self.assertEqual(response.status_code, 200)

    def test_event_using_template(self):
        response = Client().get('/kegiatan/')
        self.assertTemplateUsed(response, 'tampilan1.html')
    def test_event2_using_template(self):
        response = Client().get('/kegiatan/atur_kegiatan/')
        self.assertTemplateUsed(response, 'kegiatan.html')
    def test_event4_using_template(self):
        kegiatannya = Kegiatan.objects.create(nama_kegiatan="Lari", deskripsi_kegiatan="Lari kuy",tanggal_kegiatan="2020-03-10")
        idnya = kegiatannya.id
        response = Client().get('/kegiatan/atur_peserta/' + str(idnya) + "/")
        self.assertTemplateUsed(response, 'peserta.html')
    def test_event5_using_template(self):
        kegiatannya = Kegiatan.objects.create(nama_kegiatan="Lari", deskripsi_kegiatan="Lari kuy",tanggal_kegiatan="2020-03-10")
        idnya = kegiatannya.id
        response = Client().get('/kegiatan/detail/' + str(idnya))
        self.assertTemplateUsed(response, 'detail1.html')

class TestFunc(TestCase):
    def test_views1_func(self):
        found = resolve('/kegiatan/')
        self.assertEqual(found.func, tampilan1)
    def test_event_func(self):
        found = resolve('/kegiatan/atur_kegiatan/')
        self.assertEqual(found.func, kegiatan)
    def test_views3_func(self):
        kegiatannya = Kegiatan.objects.create(nama_kegiatan="Lari", deskripsi_kegiatan="Lari kuy",tanggal_kegiatan="2020-03-10")
        idnya = kegiatannya.id
        found = resolve('/kegiatan/detail/' + str(idnya))
        self.assertEqual(found.func, detail1)
    def test_views4_func(self):
        kegiatannya = Kegiatan.objects.create(nama_kegiatan="Lari", deskripsi_kegiatan="Lari kuy",tanggal_kegiatan="2020-03-10")
        idnya = kegiatannya.id
        found = resolve('/kegiatan/atur_peserta/' + str(idnya) +"/")
        self.assertEqual(found.func, peserta)

class TestModels(TestCase):
    def test_model_can_create(self):
        new_activity = Kegiatan.objects.create(nama_kegiatan="Lari", deskripsi_kegiatan="Lari kuy", tanggal_kegiatan="2020-03-10")
        counting_all_available_todo = Kegiatan.objects.all().count()
        self.assertEqual(counting_all_available_todo, 1)
    def test_model2_can_create(self):
        activity = Kegiatan.objects.create(nama_kegiatan="Lari", deskripsi_kegiatan="Lari kuy", tanggal_kegiatan="2020-03-10")
        peserta = Peserta.objects.create(nama="Luthfi", kegiatan=activity)
        counting_all_available_todo = Peserta.objects.all().count()
        self.assertEqual(counting_all_available_todo, 1)
    def test_model_can_print(self):
        kegiatan = Kegiatan.objects.create(nama_kegiatan="Lari", deskripsi_kegiatan="Lari kuy", tanggal_kegiatan="2020-03-10")
        self.assertEqual(kegiatan.__str__(), "Lari")
    def test_model2_can_print(self):
        activity = Kegiatan.objects.create(nama_kegiatan="Lari", deskripsi_kegiatan="Lari kuy", tanggal_kegiatan="2020-03-10")
        peserta = Peserta.objects.create(nama="Luthfi", kegiatan=activity)
        self.assertEqual(peserta.__str__(), "Luthfi")

class TestApp(TestCase):
    def test_app(self):
        self.assertEqual(KegiatanConfig.name, "kegiatan")

class TestDelete(TestCase):
    def test_delete1(self):
        activity = Kegiatan.objects.create(nama_kegiatan="Lari", deskripsi_kegiatan="Lari kuy", tanggal_kegiatan="2020-03-10")
        activity.delete()
        self.assertEqual(Kegiatan.objects.all().count(), 0)
    def test_delete2(self):
        activity = Kegiatan.objects.create(nama_kegiatan="Lari", deskripsi_kegiatan="Lari kuy", tanggal_kegiatan="2020-03-10")
        peserta = Peserta.objects.create(nama="Luthfi", kegiatan=activity)
        peserta.delete()
        self.assertEqual(Peserta.objects.all().count(), 0)