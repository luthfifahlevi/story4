from django.shortcuts import render, redirect
from .forms import Activities_Form, Peserta_Form
from .models import Kegiatan, Peserta

# Create your views here.
def tampilan1(request):
    semua_kegiatan = Kegiatan.objects.all()
    context = {
        'semua_kegiatan':semua_kegiatan
    }
    return render(request, 'tampilan1.html', context)

def kegiatan(request):
    activities_form = Activities_Form(request.POST or None)
    if request.method == "POST":
        if activities_form.is_valid():
            activities_form.save()
        return redirect('kegiatan:tampilan1')
    
    context = {
        'activities_form' : activities_form
    }
    return render(request, 'kegiatan.html', context)

def peserta(request, index):
    peserta_form = Peserta_Form(request.POST or None)
    if request.method == "POST":
        if peserta_form.is_valid():
            peserta = Peserta(kegiatan=Kegiatan.objects.get(pk = index), nama=peserta_form.data['nama'])
            peserta.save()
        x = '/kegiatan/detail/' + str(index)
        return redirect(x)
    
    context = {
        'peserta_form' : peserta_form
    }
    return render(request, 'peserta.html', context)

def delete1(request, delete_id):
    Kegiatan.objects.filter(id = delete_id).delete()
    return redirect('kegiatan:tampilan1')

def delete2(request, delete_id):
    p = Peserta.objects.get(pk = delete_id)
    p.delete()
    x = '/kegiatan/detail/' + str(p.kegiatan.id)
    return redirect(x)

def detail1(request, detail_id):
    jenis = Kegiatan.objects.get(pk = detail_id)
    semua_peserta = Peserta.objects.filter(kegiatan = jenis)
    return render(request, 'detail1.html', {'jenis': jenis, 'semua_peserta':semua_peserta})