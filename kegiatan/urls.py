from django.urls import path
from . import views

app_name = 'kegiatan'

urlpatterns = [
    path('', views.tampilan1, name='tampilan1'),
    path('atur_kegiatan/', views.kegiatan, name='kegiatan'),
    path('atur_peserta/<int:index>/', views.peserta, name='peserta'),
    path('delete/<int:delete_id>', views.delete1, name='delete1'),
    path('deletePeserta/<int:delete_id>', views.delete2, name='delete2'),
    path('detail/<int:detail_id>', views.detail1, name='detail1'),
]