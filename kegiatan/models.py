from django.db import models
from django.contrib.auth.models import User

class Kegiatan(models.Model):
    nama_kegiatan = models.CharField(max_length = 100)
    deskripsi_kegiatan = models.TextField()
    tanggal_kegiatan = models.DateField()

    def __str__(self):
        return self.nama_kegiatan

class Peserta(models.Model):
    nama = models.CharField(max_length=100)
    kegiatan = models.ForeignKey(Kegiatan, on_delete=models.CASCADE, null= True)

    def __str__(self):
        return self.nama