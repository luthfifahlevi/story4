from django import forms
from .models import Kegiatan, Peserta
# Create your views here.

class Activities_Form(forms.ModelForm):
    class Meta:
        model = Kegiatan
        fields = "__all__"
        labels = {
            'nama_kegiatan' : 'Kegiatan',
            'nama_peserta' : 'Nama',
            'deskripsi_kegiatan' : 'Deskripsi Kegiatan',
            'deskripsi_matkul' : 'Deskripsi Mata Kuliah',
            'tanggal_kegiatan': 'Tanggal Kegiatan',
        }
    
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({
                'class': 'form-control'
        }
        )
class Peserta_Form(forms.ModelForm):
    class Meta:
        model = Peserta
        fields = ['nama']
        labels = {'nama' : 'Nama Peserta'}

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({
                'class': 'form-control'
        }
        )